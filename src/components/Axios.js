import React, { PureComponent } from "react";
import axios from "axios";
import Avatar from "@mui/material/Avatar";
import "../App.css";
import { NavLink, Link } from "react-router-dom";
// import ApiPage from "./ApiPage";
export class Axios extends PureComponent {
  state = {
    user: [],
  };
  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
      console.log(res);
      this.setState({ user: res.data });
    });
  }

  render() {
    return (
      <div className="main-container">
        {this.state.user.map((elem) => (
          <NavLink
            to={{
              pathname: "/ApiPage",
            }}
            state={{ user: elem }}
            key={elem.id}
            className="avatar-div"
          >
            <div
              className="avatar-card"
              style={{
                backgroundColor: `hsl(` + Math.random() * 360 + `,  87%, 76%)`,
                clipPath: " polygon(0 0, 100% 0, 100% 76%, 0% 100%)",
              }}
            >
              <Avatar
                style={{
                  width: "70px",
                  height: "70px",
                  textDecoration: "none",
                  backgroundColor: `hsl(` + Math.random() * 360 + `, 87%, 76%)`,
                }}
                alt={elem.username}
                src={`/static/images/avatar/${elem.username}`}
              />
            </div>
            <div className="avatar-name">{elem.username}</div>
          </NavLink>
        ))}
      </div>
    );
  }
}

export default Axios;
