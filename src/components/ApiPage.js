import React from "react";
import { useLocation } from "react-router-dom";
import "../App.css";
import Avatar from "@mui/material/Avatar";

export default function ApiPage(props) {
  const location = useLocation();
  const { user } = location.state;
  return (
    <div className="dashboard-page">
      <h1 className="dashboard-name">My DashBoard</h1>
      <div className="main-user-box">
        <div className="user-per-details">
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div
              style={{
                width: "40px",
                height: "40px",
                backgroundColor: `hsl(` + Math.random() * 360 + `, 87%, 76%)`,
                clipPath:
                  "polygon(75% 0%, 100% 50%, 75% 100%, 0% 100%, 25% 50%, 0% 0%)",
              }}
            ></div>
            <h3>User's Profile </h3>
          </div>
          <Avatar
            style={{
              width: "75px",
              height: "75px",
              textDecoration: "none",
              backgroundColor: `hsl(` + Math.random() * 360 + `, 87%, 76%)`,
            }}
            alt={user.username}
            src={`/static/images/avatar/${user.username}`}
          />
          <span className="data-row">
            <label className="data">NickName :</label>
            <span className="data">{user.username}</span>
          </span>
          <span className="data-row">
            <label className="data">Name :</label>
            <span className="data">{user.name}</span>
          </span>
          <span className="data-row">
            <label className="data">Email :</label>
            <span className="data">{user.email}</span>
          </span>
        </div>
        <div className="user-details">
          <div className="user-per-details-1">
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                style={{
                  width: "40px",
                  height: "40px",
                  backgroundColor: `hsl(` + Math.random() * 360 + `, 87%, 76%)`,
                  clipPath:
                    "polygon(75% 0%, 100% 50%, 75% 100%, 0% 100%, 25% 50%, 0% 0%)",
                }}
              ></div>
              <h3>User's City </h3>
            </div>
            <span className="data-row">
              <label className="data">Area:</label>
              <span className="data">{user.address.suite}</span>
            </span>
            <span className="data-row">
              <label className="data">ZipCode :</label>
              <span className="data">{user.address.zipcode}</span>
            </span>
            <span className="data-row">
              <label className="data">City :</label>
              <span className="data">{user.address.city}</span>
            </span>
          </div>
          <div className="user-per-details-1">
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                style={{
                  width: "40px",
                  height: "40px",
                  backgroundColor: `hsl(` + Math.random() * 360 + `, 87%, 76%)`,
                  clipPath:
                    "polygon(75% 0%, 100% 50%, 75% 100%, 0% 100%, 25% 50%, 0% 0%)",
                }}
              ></div>
              <h3>User's Social </h3>
            </div>
            <span className="data-row">
              <label className="data">Phone :</label>
              <span className="data">{user.phone}</span>
            </span>
            <span className="data-row">
              <label className="data">Website :</label>
              <span className="data">{user.website}</span>
            </span>
            <span className="data-row">
              <label>Company :</label>
              <span>{user.company.name}</span>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
